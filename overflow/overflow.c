#include <stdio.h>
#include <stdlib.h>

void main(){
  int number, input, sum;

  FILE *f = fopen("/dev/urandom", "rb");
  fread(&number, sizeof(int), 1, f);
  fclose(f);
  number = abs(number);

  printf("Make this random short integer %i into a negative with a positive number and you will get the flag.\n", number);
  scanf("%i", &input);
  input = abs(input);
  sum = input+number;
  printf("The sum is %i.\n", sum);
  if (sum < 0){
    printf("9400f9e907f397f694ea1d9b62db4fe9\n", sum);
  }
}
